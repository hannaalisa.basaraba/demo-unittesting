package by.itacademy.habasaraba.unittesting;

public class Calculator {
    public int sum(int a, int b) {
        int result = 0;
        if ((0 <= a) && (a < 10) && (0 <= b) && (b < 10)) result = a + b;

        if ((10 <= a) && (a < 20) && (10 <= b) && (b < 20) && ((20 <= (a + b)) && ((a + b) < 30))) result = 20;

        if ((10 <= a) && (a < 20) && (10 <= b) && (b < 20) && (((30 <= (a + b)) && ((a + b) < 40)))) result = 30;

        if (a >= 20 && b >= 20) result = 40;

        if (a < 0 || b < 0) result = 0;
        return result;
    }

    /*
    0-9
    10-14
    15-19
    20+
    >0
    */
    public int divide(int a, int b) {
        int result = 0;
        if((b <= 0)) result = 0;
        if((a >= b)) result = a / b;
        if ((a < b)) result = 1;
        return result;
    }

    public int multiply(int a, int b){
        int result=0;
        if((a<0)||(b<0)) result=-1;
        if((a>=0)&&(a<10)&&(b>=0)&&(b<10)) result=a*b;
        if((a>=10)&&(a<100)&&(b>=10)&&(b<100)&&(a*b)>=100&&(a*b)<1000) result=100;
        if((a>=10)&&(a<100)&&(b>=10)&&(b<100)&&(a*b)>=1000&&(a*b)<10000) result=1000;
        if((a>=100)||(b>=100)) result=10000;
        return result;
    }
    /*
    >0
    0
    0-9
    10-31
    32-100
    100+
     */
    public int subtract(int a, int b) {
        int result = 0;
        if((a>=b)&&(a>=0)&&(b>=0)) result = a-b;
        if((a>=b)&&(a>=0)&&(b<0)) result = a;
        if (a<b) result = -b;
        return result;
    }



}



