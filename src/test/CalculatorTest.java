import by.itacademy.habasaraba.unittesting.Calculator;
import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {

    @Test
    public void testSum1() {
        Calculator calc = new Calculator();
        int actual = calc.sum(-1, 8);
        Assert.assertEquals(0,actual);
    }
    @Test
    public void testSum2() {
        Calculator calc = new Calculator();
        int actual = calc.sum(0, 9);
        Assert.assertEquals(9,actual);
    }
    @Test
    public void testSum3() {
        Calculator calc = new Calculator();
        int actual = calc.sum(1, 9);
        Assert.assertEquals(10,actual);
    }
    @Test
    public void testSum4() {
        Calculator calc = new Calculator();
        int actual = calc.sum(5, 8);
        Assert.assertEquals(13,actual);
    }
    @Test
    public void testSum5() {
        Calculator calc = new Calculator();
        int actual = calc.sum(10, 14);
        Assert.assertEquals(20,actual);
    }
    @Test
    public void testSum6() {
        Calculator calc = new Calculator();
        int actual = calc.sum(11, 12);
        Assert.assertEquals(20,actual);
    }
    @Test
    public void testSum7() {
        Calculator calc = new Calculator();
        int actual = calc.sum(15, 19);
        Assert.assertEquals(30,actual);
    }
    @Test
    public void testSum8() {
        Calculator calc = new Calculator();
        int actual = calc.sum(16, 18);
        Assert.assertEquals(30,actual);
    }
    @Test
    public void testSum9() {
        Calculator calc = new Calculator();
        int actual = calc.sum(20, 20);
        Assert.assertEquals(40,actual);
    }
    @Test
    public void testSum10() {
        Calculator calc = new Calculator();
        int actual = calc.sum(21, 47);
        Assert.assertEquals(40,actual);
    }
    @Test
    public void testSum11() {
        Calculator calc = new Calculator();
        int actual = calc.sum(-2, -9);
        Assert.assertEquals(0,actual);
    }
    @Test
    public void testSum12() {
        Calculator calc = new Calculator();
        int actual = calc.sum(-2, -9);
        Assert.assertEquals(0, actual);
    }

    @Test
    public void testSum13() {
        Calculator calc = new Calculator();
        int actual = calc.sum(0, 10);
        Assert.assertEquals(0, actual);
    }
    @Test
    public void testDivide1(){
        Calculator calc=new Calculator();
        int actual= calc.divide(4,2);
        Assert.assertEquals(2,actual);
    }
    @Test
    public void testDivide2(){
        Calculator calc=new Calculator();
        int actual= calc.divide(8,2);
        Assert.assertEquals(4,actual);
    }
    @Test
    public void testDivide3(){
        Calculator calc=new Calculator();
        int actual= calc.divide(8,8);
        Assert.assertEquals(1,actual);
    }
    @Test
    public void testDivide4(){
        Calculator calc=new Calculator();
        int actual= calc.divide(2,8);
        Assert.assertEquals(1,actual);
    }

    @Test
    public void testMultiply1(){
        Calculator calc=new Calculator();
        int actual= calc.multiply(-9,8);
        Assert.assertEquals(-1,actual);
    }
    @Test
    public void testMultiply2(){
        Calculator calc=new Calculator();
        int actual= calc.multiply(0,8);
        Assert.assertEquals(0,actual);
    }
    @Test
    public void testMultiply3(){
        Calculator calc=new Calculator();
        int actual= calc.multiply(0,9);
        Assert.assertEquals(0,actual);
    }
    @Test
    public void testMultiply4(){
        Calculator calc=new Calculator();
        int actual= calc.multiply(1,8);
        Assert.assertEquals(8,actual);
    }
    @Test
    public void testMultiply5(){
        Calculator calc=new Calculator();
        int actual= calc.multiply(9,9);
        Assert.assertEquals(81,actual);
    }
    @Test
    public void testMultiply6(){
        Calculator calc=new Calculator();
        int actual= calc.multiply(9,10);
        Assert.assertEquals(0,actual);
    }
    @Test
    public void testMultiply7(){
        Calculator calc=new Calculator();
        int actual= calc.multiply(11,12);
        Assert.assertEquals(100,actual);
    }
    @Test
    public void testMultiply8(){
        Calculator calc=new Calculator();
        int actual= calc.multiply(11,31);
        Assert.assertEquals(100,actual);
    }
    @Test
    public void testMultiply9(){
        Calculator calc=new Calculator();
        int actual= calc.multiply(11,32);
        Assert.assertEquals(100,actual);
    }
    @Test
    public void testMultiply10(){
        Calculator calc=new Calculator();
        int actual= calc.multiply(32,32);
        Assert.assertEquals(1000,actual);
    }
    @Test
    public void testMultiply11(){
        Calculator calc=new Calculator();
        int actual= calc.multiply(31,31);
        Assert.assertEquals(100,actual);
    }
    @Test
    public void testMultiply12(){
        Calculator calc=new Calculator();
        int actual= calc.multiply(31,32);
        Assert.assertEquals(100,actual);
    }
    @Test
    public void testMultiply13(){
        Calculator calc=new Calculator();
        int actual= calc.multiply(33,45);
        Assert.assertEquals(1000,actual);
    }
    @Test
    public void testMultiply14(){
        Calculator calc=new Calculator();
        int actual= calc.multiply(100,100);
        Assert.assertEquals(10000,actual);
    }
    @Test
    public void testMultiply15(){
        Calculator calc=new Calculator();
        int actual= calc.multiply(100,99);
        Assert.assertEquals(10000,actual);
    }
    @Test
    public void testSubtract1(){
        Calculator calc=new Calculator();
        int actual= calc.subtract(0,0);
        Assert.assertEquals(0,actual);
    }
    @Test
    public void testSubtract2(){
        Calculator calc=new Calculator();
        int actual= calc.subtract(1,0);
        Assert.assertEquals(1,actual);
    }
    @Test
    public void testSubtract3(){
        Calculator calc=new Calculator();
        int actual= calc.subtract(1,1);
        Assert.assertEquals(0,actual);
    }
    @Test
    public void testSubtract4(){
        Calculator calc=new Calculator();
        int actual= calc.subtract(2,4);
        Assert.assertEquals(-4,actual);
    }
    @Test
    public void testSubtract5(){
        Calculator calc=new Calculator();
        int actual= calc.subtract(3,-1);
        Assert.assertEquals(3,actual);
    }


}




